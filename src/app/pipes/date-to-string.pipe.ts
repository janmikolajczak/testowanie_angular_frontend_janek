import { UtilsService } from 'src/app/services/utils.service';
import { Pipe, PipeTransform } from '@angular/core';


const DEFAULT_TEMPLATE = 'yyyy-MM-dd HH:mm';


@Pipe({
  name: 'dateToString'
})
export class DateToStringPipe implements PipeTransform {

  constructor(
    private readonly utils: UtilsService
  ) {}

  transform(value: string | number | Date | undefined | null, template: string = DEFAULT_TEMPLATE): string {
    if (!template) template = DEFAULT_TEMPLATE;
    
    const date = this.utils.newDate(value);
    if (date.toString() === 'Invalid Date') return '';
    
    const yyyy = '' + date.getFullYear();
    const yyy = yyyy.substring(1);
    const yy = yyy.substring(1);
    const y = yy.substring(1);

    const M = '' + (date.getMonth() + 1);
    const MM = M.length === 2 ? M : `0${M}`;

    const d = '' + date.getDate();
    const dd = d.length === 2 ? d : `0${d}`;

    const H = '' + date.getHours();
    const HH = H.length === 2 ? H : `0${H}`;

    const h = '' + (+H > 12 ? +H - 12 : H);
    const hh = h.length === 2 ? h : `0${h}`;

    const m = '' + date.getMinutes();
    const mm = m.length === 2 ? m : `0${m}`;

    const s = '' + date.getSeconds();
    const ss = s.length === 2 ? s : `0${s}`;

    let str: string = template;

    str = this.replaceAll(str)('yyyy', yyyy);
    str = this.replaceAll(str)('yyy', yyy);
    str = this.replaceAll(str)('yy', yy);
    str = this.replaceAll(str)('y', y);

    str = this.replaceAll(str)('MM', MM);
    str = this.replaceAll(str)('M', M);

    str = this.replaceAll(str)('dd', dd);
    str = this.replaceAll(str)('d', d);

    str = this.replaceAll(str)('HH', HH);
    str = this.replaceAll(str)('H', H);

    str = this.replaceAll(str)('hh', hh);
    str = this.replaceAll(str)('h', h);

    str = this.replaceAll(str)('mm', mm);
    str = this.replaceAll(str)('m', m);

    str = this.replaceAll(str)('ss', ss);
    str = this.replaceAll(str)('s', s);

    return str;
  }

  private replaceAll(template): (searchValue: string | RegExp, replaceValue: string) => string {
    return function (searchValue: string | RegExp, replaceValue: string): string {
      while (template.includes(searchValue)) {
        template = template.replace(searchValue, replaceValue);
      }

      return template;
    }
  }
}