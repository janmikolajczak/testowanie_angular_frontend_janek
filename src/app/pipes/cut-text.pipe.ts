import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cutText'
})
export class CutTextPipe implements PipeTransform {

  transform(value: string, lengthBeforeCut: number = 20): string {
    if (!value?.length) return value;
    if (value.length <= lengthBeforeCut) return value;
    return value.substr(0, lengthBeforeCut) + '...';
  }
}