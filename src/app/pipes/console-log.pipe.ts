import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'consoleLog'
})
export class ConsoleLogPipe implements PipeTransform {

  transform<T>(value: T): T {
    console.log('--- consoleLog pipe ---');
    console.log(value);
    console.log('--- END consoleLog pipe ---');
    return value;
  }

}
