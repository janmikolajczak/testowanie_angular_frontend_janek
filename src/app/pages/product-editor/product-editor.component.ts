import { Component, OnInit, OnDestroy } from '@angular/core';
import { BasicService } from 'src/app/services/basic.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AlertsService } from 'src/app/services/alert.service';
import { IHelper, ValidatorService } from 'src/app/bye/validator.service';
import { ViewChild } from '@angular/core';
import { ElementRef } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NavigationService } from 'src/app/services/navigation.service';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'app-product-editor',
  templateUrl: './product-editor.component.html',
  styleUrls: ['./product-editor.component.scss']
})

export class ProductEditorComponent implements OnInit, OnDestroy {

  public loading: boolean = false;
  public title: string = 'PRODUCT_EDITOR_TITLE_TEXT'

  public edited_item_index: number;
  public model: any = {};
  public helper: IHelper = {};
  public additional: any = {
  };

  constructor(public basic: BasicService, public http: HttpClient, public route: ActivatedRoute, private router: Router, private translator: TranslateService, private validator: ValidatorService, private alert: AlertsService, private activeRouter: ActivatedRoute, private modalService: NgbModal, public navigation: NavigationService, private loader: LoaderService) { }

  ngOnInit() {
    this.navigation.setTitle(this.title);
    this.activeRouter.queryParamMap
      .subscribe((params: any) => {
        const id = params.params.id;
        this.getData(id);
    });
  }

  ngOnDestroy() {
    this.loading = false;
    this.loader.state = {
      showLoader: true
    }
  }

  getData(id: number) {
    const req = {
      metadata: {
        token: this.basic.getToken()
      },
      data: {
        id: id
      }
    }

    this.loading = true;
    this.loader.state = {
      showLoader: true
    };

    this.http.post(this.basic.getBaseUrl() + '/web/product_editor/GetData', req).subscribe(res => {
      this.loading = false;
      this.loader.state = {
        showLoader: false
      };

      try {
        const status = this.basic.processResponse(res);
        if (status === 'OK') {
          const data = res['data'];
          if (!data) return;
          const m = data['model'];
          if (m !== undefined) this.model = m;
          const a = data['additional'];
          if (a !== undefined) this.additional = a;
        }
      } catch (exception) {
        this.router.navigate(['/login']);
      }
    }, err => {
      this.loading = false;
      this.loader.state = {
        showLoader: false
      };

    });
  }

  saveAndUpdateModel() {
    const servlet = '/web/product_editor/SaveData';
    if (!this.validator.strictValidate(this.model as any, this.helper as any)) {
      return;
    }

    const req = {
      metadata: {
        token: this.basic.getToken()
      },
      data: this.model
    };

    this.loading = true;

    this.loader.state = {
      showLoader: true
    };

    this.http.post(this.basic.getBaseUrl() + servlet, req).subscribe(res => {
      this.loading = false;

      this.loader.state = {
        showLoader: false
      };

      const status = this.basic.processResponse(res);
      if (status === 'OK') {
        this.alert.showTimeout('SAVE_DATA_SUCCESS');
        const data = res['data'];
        if (data !== undefined) this.model = data;
        this.router.navigate(['/list_of_products']);
        return;
      } else {
        this.alert.showError('', status);
      }
    }, err => {
      this.loading = false;
      this.loader.state = {
        showLoader: false
      };

    });
  }
}
