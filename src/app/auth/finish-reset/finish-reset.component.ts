import { BasicService } from './../../services/basic.service';
import { AlertsService } from './../../services/alert.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthService } from './../auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'app-finish-reset',
  templateUrl: './finish-reset.component.html',
  styleUrls: ['./finish-reset.component.scss']
})
export class FinishResetComponent implements OnInit, OnDestroy {
  subs$: Subscription[] = [];
  form!: FormGroup;
  loading = false;

  constructor(
    private route: ActivatedRoute,
    private auth: AuthService,
    private loaderService: LoaderService,
    private alert: AlertsService,
    private router: Router,
    private basic: BasicService
  ) {}

  ngOnInit(): void {
    // jeśli hash będzie przekazywany jako parametr (a nie query parametr), trzeba niżej "queryParams" podmienić na "params" i w "app.routing.ts" trzeba 'finish_reset' zamienić na 'finish_reset/:hash'
    const s$ = this.route.queryParams
      .subscribe((qp: any) => {
        if (!qp.hash) return;
        this.form = new FormGroup({
          hash: new FormControl(qp.hash, [Validators.required]),
          pass: new FormControl('', [
            Validators.required, Validators.minLength(6)
          ]),
        });
      });

    this.subs$.push(s$);

    this.loaderService.state = {
      showLoader: false
    }
  }

  finishReset() {
    if (this.form.invalid) return;
    this.loading = true;
    this.loaderService.state = {
      showLoader: true
    }

    this.auth.finishReset({...this.form.value})
      .subscribe(ok => {
        this.loading = false;
        this.loaderService.state = {
          showLoader: false
        }
        if (!ok) {
          this.alert.showError('FINISH_RESET_FAIL_MESSAGE_1', 'FINISH_RESET_FAIL_MESSAGE_2');
          return;
        }
        
        const role = this.basic.getUserRole();
        this.form.reset();
        this.router.navigate(['/' + role + '_dashboard']);
        this.alert.success('FINISH_RESET_SUCCESS');
      }, err => {
        console.error(err);
        this.loading = false;
        this.loaderService.state = {
          showLoader: false
        }

        this.alert.showError('FINISH_RESET_FAIL_MESSAGE_1', 'FINISH_RESET_FAIL_MESSAGE_2');
      });
  }

  ngOnDestroy(): void {
    this.subs$.forEach(s$ => {
      try {
        s$ && s$.unsubscribe();
      } catch (_) {}
    });
  }
}
