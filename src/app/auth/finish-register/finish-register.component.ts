import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { BasicService } from '../../services/basic.service';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'app-finish-register',
  templateUrl: './finish-register.component.html',
  styleUrls: ['./finish-register.component.css']
})
export class FinishRegisterComponent implements OnInit, OnDestroy {
  token: string;
  status: string;

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private router: Router,
    public readonly basic: BasicService,
    private loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.route.queryParams
      .subscribe((qp: any) => {
        if (qp?.token) {
          this.token = qp.token;
          this.finishRegister();
        }
      });
  }

  finishRegister() {
    const req = {
      metadata: {},
      data: {
        token: this.token
      }
    }

    this.loaderService.state = {
      showLoader: true
    }

    this.http.post(`${this.basic.getBaseUrl()}/FinishRegister`, req)
      .subscribe((res: any) => {
        if (res?.metadata?.status !== 'OK') {
          this.router.navigate(['/register']);
          return;
        }
        this.status = 'OK';

        this.loaderService.state = {
          showLoader: false
        }
      }, err => {
        this.router.navigate(['/register']);
        this.loaderService.state = {
          showLoader: false
        }
      });
  }

  ngOnDestroy(): void {
    this.loaderService.state = {
      showLoader: false
    }
  }
}