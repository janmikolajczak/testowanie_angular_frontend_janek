import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

type Action = 'CLEAR' | 'OPEN_LOGIN' | 'OPEN_REGISTER';

@Injectable({
  providedIn: 'root'
})
export class AuthModalsService {
  private action: Action = 'CLEAR';
  private action$: Subject<Action> = new BehaviorSubject<Action>(this.action);

  constructor() {}

  getAction(): Observable<Action> {
    return this.action$.asObservable();
  }

  setAction(action: Action) {
    this.action = action;
    this.action$.next(this.action);
  }
}