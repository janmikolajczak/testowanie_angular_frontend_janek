import { LoaderService } from './../../services/loader.service';
import { BasicService } from 'src/app/services/basic.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-unfinished-registration',
  templateUrl: './unfinished-registration.component.html',
  styleUrls: ['./unfinished-registration.component.css']
})
export class UnfinishedRegistrationComponent implements OnInit {

  constructor(
    public readonly basic: BasicService,
    private loaderService: LoaderService
  ) {}

  ngOnInit(): void {
    this.loaderService.state = {
      showLoader: false
    }
  }
}
