import { TranslateService } from '@ngx-translate/core';
import { BasicService } from 'src/app/services/basic.service';
import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

export type Lang = 'pl' | 'en' | 'fr' | 'uk' | 'sw' | 'ar' | 'de' | 'es' | 'vi' | 'zh' | 'ko' | 'tr';

export interface ILang {
  img: string
  name: string
}

export interface ILangs {
  [lang: string]: ILang
}

function getLangFromLocalStorage(): Lang {
  const lang: Lang | string = localStorage.getItem('lang');
  switch(lang) {
    case 'pl':
    case 'en':
    case 'fr':
    case 'uk':
    case 'sw':
    case 'ar':
    case 'de':
    case 'es':
    case 'vi':
    case 'zh':
    case 'ko':
    case 'tr':
      return lang;

    default:
      return 'pl';
  }
}


@Injectable({
  providedIn: 'root'
})
export class LangService implements OnDestroy {
  private selectedLang: Lang = getLangFromLocalStorage();

  private langStream$: Subject<Lang> = new BehaviorSubject<Lang>(this.selectedLang);
  private langNames: Lang[] = ['pl'];
  private langs: ILangs = {};


  constructor(
    private readonly basic: BasicService,
    private translate: TranslateService
  ) {
    this.langNames.forEach(x => {
      this.langs[x] = {
        img: `${this.basic.getBaseUrl()}/assets/img/flags/${x}-flag.jpg`,
        name: x.toUpperCase()
      }
    });

    const lang: Lang = getLangFromLocalStorage();
    this.translate.setDefaultLang(lang);
    this.setLang(lang);
  }

  getLangs(): ILangs {
    return this.langs;
  }

  getCurrentLang(): Lang {
    return this.selectedLang;
  }

  getLang$(): Observable<Lang> {
    return this.langStream$;
  }

  setLang(lang: Lang): void {
    localStorage.setItem('lang', lang);
    this.selectedLang = lang;
    this.translate.use(lang);

    this.langStream$.next(this.selectedLang);
  }

  ngOnDestroy(): void {
    this.langStream$.unsubscribe();
  }
}