import { Injectable } from '@angular/core';
import { RealTypeService } from './real-type.service';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  private symbols: string[] = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M'];

  constructor(
    private readonly realType: RealTypeService
  ) {}

  getRandomInt(min: number, max: number): number {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

  generateHash(len: number = 16): string {
    let hash: string = '';

    for (let i = 0; i < len; i++) {
      hash += this.symbols[this.getRandomInt(0, this.symbols.length)]
    }

    return hash;
  }

  copy<T>(x: T): T {
    if (this.realType.isFunction(x)) throw new Error('Can not copy function');

    if (!x) return x;
    if (typeof(x) !== 'object') return x;
    
    return JSON.parse(JSON.stringify(x));
  }

  newDate(value?: string | number | Date | undefined | null): Date {
    if (!arguments.length) return new Date();
    
    const regExp = /^[0-9]{4}\-[0-9]{2}\-[0-9]{2} [0-9]{2}\:[0-9]{2}\:[0-9]{2}/;
    if (typeof(value) === 'string' && regExp.test(value)) {
      let [valueDate, valueTime] = value.split(' ');
      valueTime = valueTime.split('.')[0];
      const [year, month, day] = valueDate.split('-');
      const [hour, minute, second] = valueTime.split(':');
      return new Date(+year, +month - 1, +day, +hour, +minute, +second);
    }

    return new Date(value);
  }

  encodeURI(uri: string): string {
    return encodeURI(uri);
  }

  decodeURI(uri: string): string {
    return this.decodeURI(uri);
  }
}