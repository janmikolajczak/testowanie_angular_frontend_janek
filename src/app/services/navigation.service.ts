import { TranslateService } from '@ngx-translate/core';
import { Injectable, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { TITLE } from '../models/constants';

type numStr = number | string;

export interface ISubNavLink {
  linkTo: string | numStr[],
  title: string,
  iconClass?: string,
  roleRegExp?: string | RegExp
}

export interface INavLink {
  linkTo?: string | numStr[],
  title: string,
  iconClass?: string,
  sub?: ISubNavLink[]
}

@Injectable({
  providedIn: 'root'
})
export class NavigationService implements OnDestroy {
  private sub: ISubNavLink[] = [];
  private title: string = '';

  private translatorIsLoaded: boolean = false;
  private needSetDocumentTitle: boolean = false;

  subLinkStream$: Subject<ISubNavLink[]> = new BehaviorSubject<ISubNavLink[]>(this.sub);
  titleStream$: Subject<string> = new BehaviorSubject<string>(this.title);

  menuToggleClickEmiter$: Subject<void> = new BehaviorSubject<void>(null);

  constructor(
    private documentTitle: Title,
    private readonly translator: TranslateService
  ) {
    this.translator.onLangChange.subscribe(_ => {
      this.translatorIsLoaded = true;

      if (this.needSetDocumentTitle) {
        this.setDocumentTitle();
      }
    });
  }

  getSubLinks$(): Observable<ISubNavLink[]> {
    return this.subLinkStream$;
  }

  setSubLinks(subLinks: ISubNavLink[]): void {
    this.sub = subLinks;
    this.subLinkStream$.next(this.sub);
  }

  getTitle$(): Observable<string> {
    return this.titleStream$;
  }

  setTitle(title: string): void {
    this.title = title;
    this.titleStream$.next(this.title);

    if (this.translatorIsLoaded) {
      this.setDocumentTitle();
    } else {
      this.needSetDocumentTitle = true;
    }
  }

  emitMenuToggle() {
    this.menuToggleClickEmiter$.next();
  }

  private setDocumentTitle() {
    this.documentTitle.setTitle(`${TITLE} | ${this.translator.instant(this.title)}`);
  }

  ngOnDestroy(): void {
    this.subLinkStream$.unsubscribe();
    this.titleStream$.unsubscribe();
    this.menuToggleClickEmiter$.unsubscribe();
  }

}
