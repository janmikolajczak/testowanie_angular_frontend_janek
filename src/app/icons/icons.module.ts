import { IconListComponent } from './list/list.component';
import { IconGridComponent } from './grid/grid.component';
import { IconSearchComponent } from './search/search.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChevronDownComponent } from './chevron-down/chevron-down.component';
import { ChevronUpComponent } from './chevron-up/chevron-up.component';



@NgModule({
  declarations: [
    IconSearchComponent,
    IconGridComponent,
    IconListComponent,
    ChevronDownComponent,
    ChevronUpComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    IconSearchComponent,
    IconGridComponent,
    IconListComponent,
    ChevronDownComponent,
    ChevronUpComponent
  ]
})
export class IconsModule { }
