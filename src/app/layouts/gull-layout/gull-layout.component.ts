import { AuthService } from "src/app/auth/auth.service";
import { BasicService } from "src/app/services/basic.service";
import { NavigationService, ISubNavLink } from "src/app/services/navigation.service";

import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  HostListener,
  ChangeDetectorRef,
  ViewEncapsulation,
} from "@angular/core";
import { INavItem, NavLinksService } from "src/app/services/nav-links.service";


interface IMenuState {
  menuOpen: boolean;
  subMenuOpen: boolean;
}


@Component({
  selector: 'app-gull-layout',
  templateUrl: './gull-layout.component.html',
  styleUrls: ['./gull-layout.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GullLayoutComponent implements OnInit {
  @ViewChild("menu") menuRef: ElementRef<HTMLElement>;
  @ViewChild("subMenu") subMenuRef: ElementRef<HTMLElement>;

  title: string = "";

  menuOpen: boolean = true; // Czy menu otwarte
  subMenuOpen: boolean = false; // Czy submenu otwarte
  dropdownOpen: boolean = false; // Czy dropdown otwarty
  subMenuContent: ISubNavLink[] = [];
  BP: number = 767; // Granica zmian na desktop / tel
  selectedKey: string = null;
  navItems: INavItem[] = [];

  constructor(
    private readonly navigationService: NavigationService,
    public basic: BasicService,
    private cdref: ChangeDetectorRef,
    private readonly auth: AuthService,
    private navLinksService: NavLinksService
  ) {}

  ngOnInit(): void {
    this.navItems = this.navLinksService.links;

    this.getSubMenuContent();
    this.navigationService.getTitle$().subscribe(
      (title) => {
        this.title = title;
        this.cdref.detectChanges();
      },
      (err) => console.error(err)
    );

    this.navigationService.menuToggleClickEmiter$.subscribe(
      (_) => {
        this.hideSubmenu();
      },
      (err) => console.error(err)
    );
  }

  @HostListener("window:load", [])
  windowOnLoadController() {
    this.checkWidth();
  }

  @HostListener("window:resize", [])
  windowOnResizeConstroller() {
    this.checkWidth();
  }

  checkWidth(e?: MouseEvent | any): void {
    const w = window.innerWidth; // Szerokość ekranu
    let a, b;

    if (e && e.path && e.path.length > 0) {
      const a = e.path.find((el) => el.id === "menu_btn");

      if (a) return;
    }

    if (e) {
      if (e.path && e.path.length > 0) {
        a = e.path.find((el) => el.id === "menu_btn");
        b = e.path.find((el) => el.id === "submenu");
      } else if (e.composedPath) {
        const path = e.composedPath();
        if (!path || path.length === 0) return;

        a = path.find((el) => el.id === "menu_btn");
        b = path.find((el) => el.id === "submenu");
      }

      if (a || b) return;
    }

    if (w > this.BP) {
      this.setMenuState({
        menuOpen: true,
        subMenuOpen: false,
      });
    } else {
      this.setMenuState({
        menuOpen: false,
        subMenuOpen: false,
      });
    }
  }

  hideSubmenu() {
    const w = window.innerWidth; // Szerokość ekranu

    if (w > this.BP) {
      this.subMenuOpen = false;
    } else {
      this.setMenuState({
        menuOpen: false,
        subMenuOpen: false,
      });
    }
  }

  setMenuState(state: IMenuState): void {
    this.menuOpen = state.menuOpen;
    this.subMenuOpen = state.subMenuOpen;
  }

  changeMenuState(e?: Event | any): void {
    this.navigationService.setSubLinks([]);

    if (this.menuOpen && this.subMenuOpen) {
      this.setMenuState({
        menuOpen: true,
        subMenuOpen: false,
      });
    } else if (this.menuOpen && !this.subMenuOpen) {
      this.setMenuState({
        menuOpen: false,
        subMenuOpen: false,
      });
    } else if (!this.menuOpen && !this.subMenuOpen) {
      if (this.subMenuContent && this.subMenuContent.length > 0) {
        this.openMenu();
      } else {
        this.setMenuState({
          menuOpen: true,
          subMenuOpen: false,
        });
      }
    }

    e?.preventDefault && e.preventDefault();
  }

  openMenu() {
    if (this.subMenuContent && this.subMenuContent.length > 0) {
      this.setMenuState({
        menuOpen: true,
        subMenuOpen: true,
      });
    } else {
      this.setMenuState({
        menuOpen: true,
        subMenuOpen: false,
      });
    }
  }

  dropdownToggle(): void {
    this.dropdownOpen = !this.dropdownOpen;
  }

  getSubMenuContent(): void {
    this.navigationService.getSubLinks$().subscribe(
      (sub) => {
        this.subMenuContent = sub;
      },
      (err) => {
        console.error(err);
      }
    );
  }

  logout(): void {
    this.dropdownOpen = false;
    this.auth.logout();
  }

  public isGroupVisible(condition: string): boolean | string {
    if (condition === undefined) return false;
    const r = new RegExp(condition);
    return r.test(this.basic.getUserRole());
  }

  selectedKeyChanged(e: string) {
    if (!e) {
      this.setMenuState.bind(this, {
          menuOpen: false,
          subMenuOpen: false,
        });
    }
  }
}
