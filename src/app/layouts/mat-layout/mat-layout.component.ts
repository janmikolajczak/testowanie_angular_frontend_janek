import { BasicService } from './../../services/basic.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ChangeDetectorRef, Component, HostListener, OnInit, OnDestroy } from '@angular/core';
import { NavigationService } from 'src/app/services/navigation.service';
import { INavItem, ISubNavItem, NavLinksService } from 'src/app/services/nav-links.service';


const BP = 1200;


@Component({
  selector: 'app-mat-layout',
  templateUrl: './mat-layout.component.html',
  styleUrls: ['./mat-layout.component.scss']
})
export class MatLayoutComponent implements OnInit, OnDestroy {
  menuIsOpen: boolean = false;
  dropdownIsOpen: boolean = false;

  title: string = "";
  links: INavItem[] = this.navLinksService.links;
  subs: ISubNavItem[] = [];
  subscriptions$: Subscription[] = [];

  constructor(
    private readonly navigationService: NavigationService,
    private cdref: ChangeDetectorRef,
    private navLinksService: NavLinksService,
    private router: Router,
    private basic: BasicService
  ) {}

  ngOnInit(): void {
    this.navigationService.getTitle$()
      .subscribe(title => {
        this.title = title;
        this.cdref.detectChanges();
      });

    const s$: Subscription = this.navLinksService.links$
      .subscribe(links => {
        this.links = links;
        this.subs = this.links.find(link => link.active)?.sub || [];
      });
    
    this.subscriptions$.push(s$);
  }

  @HostListener('window:click', ['$event'])
  onWindowClick(e: MouseEvent) {
    let path: HTMLElement[] = (e as any)?.path?.length || [];
    if ((e as any)?.path) {
      path = (e as any)?.path;
    } else if (e.composedPath) {
      path = e.composedPath() as HTMLElement[];
    }

    if (!path || path.length === 0) return;
    const dropdown = path.find(x => x?.classList?.contains && x.classList.contains('mat_layout_header_dropdown'));
    const btn = path.find(x => x?.classList?.contains && x.classList.contains('mat_layout_header_other_btn'));
    if (dropdown || btn) return;
    this.dropdownIsOpen = false;
  }

  closeMenu() {
    this.menuIsOpen = false;
  }

  toggleMenu() {
    this.menuIsOpen = !this.menuIsOpen;
  }

  toggleDropdown() {
    this.dropdownIsOpen = !this.dropdownIsOpen;
  }

  goToLink(link: INavItem) {
    link.sub = link.sub || [];
    for (let i = 0, l = link.sub.length; i < l; i++) {
      const s = link.sub[i];
      if (s.linkTo?.length) {
        this.router.navigate(s.linkTo);
        this.closeMenu();
        break;
      }
    }
  }

  logout() {
    this.dropdownIsOpen = false;
    this.basic.logout()
      .finally(() => this.router.navigate(['/login']));
  }

  ngOnDestroy(): void {
    this.subscriptions$.forEach(s$ => {
      try {
        s$ && s$.unsubscribe();
      } catch (_) {}
    })
  }
}
