import { ColorSchemeService } from './services/color-scheme.service';
import { Router } from '@angular/router';
import { AlertsService } from 'src/app/services/alert.service';
import { Subscription } from 'rxjs';
import { GlobalActions } from './classes/GlobalActions';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { BasicService } from './services/basic.service';


@Component({
	selector: 'app',
	templateUrl: 'app.component.html',
	styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit, OnDestroy {
	subs$: Subscription[] = [];

	constructor(
		private readonly basic: BasicService,
		public translate: TranslateService,
		private alert: AlertsService,
		private router: Router,
		private colorSchemeService: ColorSchemeService
	) {
		translate.setDefaultLang('pl');
		translate.use('pl');
	}

	ngOnInit() {
		this.colorSchemeService.init();
		this.subscribeGlobalActions();
		const basic = this.basic;

		window.addEventListener("beforeunload", function (e) {
			// Zapis tokenu na chwilę (od startu do zakończenia przeładowania strony)
			localStorage.setItem("token",basic.getToken());
			localStorage.setItem("role",basic.getUserRole());
		});

		
	}

	subscribeGlobalActions() {
		const fn = _ => {
			this.router.navigate(['/login']);
			
			this.alert.showInfo(
				this.translate.instant('MAIN_PASS_NOT_FOUND_TITLE'), 
				this.translate.instant('MAIN_PASS_NOT_FOUND_TEXT')
				);
		}

		const s$ = GlobalActions.state$
			.subscribe(state => {
				if (state.action === 'MAIN_PASS_NOT_FOUND') {
					this.basic.logout()
						.then(fn)
						.catch(fn);
				}
			});

		this.subs$.push(s$);
	}

	ngOnDestroy(): void {
		this.subs$.forEach(s$ => {
			try {
				s$ && s$.unsubscribe();
			} catch (_) {}
		});
	}
}