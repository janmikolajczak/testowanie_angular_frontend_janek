import { HelperType, ValidateOn, IHelper, ValidatorService, IHelperItem } from './../validator.service';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-byetext',
  templateUrl: './byetext.component.html',
  styleUrls: ['./byetext.component.scss']
})
export class ByetextComponent implements OnChanges, OnInit {
  @Input() model: any;
  @Input() helper: IHelper;
  @Input() key: string;
  @Input() valid: HelperType;
  @Input() disable = false;
  @Input() placeholder: string = '';
  @Input() className: string = '';
  @Input() validateOn: ValidateOn[] = ['modelChange'];
  @Input() showComponentStyles: boolean = true;

  @Output() OnBlur: EventEmitter<Event | any> = new EventEmitter<Event | any>();
  @Output() OnFocus: EventEmitter<Event | any> = new EventEmitter<Event | any>();
  @Output() OnInput: EventEmitter<Event | any> = new EventEmitter<Event | any>();
  @Output() OnModelChanged: EventEmitter<Event | any> = new EventEmitter<Event | any>();

  constructor(
    private validator: ValidatorService,
    private utils: UtilsService
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.valid && this.helper && this.key && this.helper[this.key]) {
      const helperItem: IHelperItem = this.utils.copy(this.helper[this.key]);
      
      this.helper[this.key] = {
        type: this.valid,
        valid: helperItem?.valid || 'UNKNOWN',
        hide: helperItem?.hide || false,
        wasFocused: helperItem?.wasFocused || false,
        lostFocus: helperItem?.lostFocus || false,
        wasChanged: helperItem?.wasChanged || false,
        modelWasChanged: helperItem?.modelWasChanged || false,
        isValid: helperItem?.isValid || false,
        isInvalid: helperItem?.isInvalid || false
      }

      if (this.validateOn.includes('modelChange') || this.validateOn.includes('input')) {
        this.validate();
      }
    }
  }

  ngOnInit() {
    if (this.helper && this.key) {
      this.helper[this.key] = {
        type: this.valid,
        valid: 'UNKNOWN',
        hide: false,
        wasFocused: false,
        lostFocus: false,
        wasChanged: false,
        modelWasChanged: false,
        isValid: false,
        isInvalid: false
      };

      if (this.validateOn.includes('init')) {
        this.validate();
      }
    }
  }

  onBlur(e?: Event | any) {
    this.OnBlur.emit(e);
    if(!this.helper) this.helper = {};
    if(!this.helper[this.key]) this.helper[this.key] = {};
    this.helper[this.key].lostFocus = true;

    if (this.validateOn.includes('blur')) {
      this.validate();
    }
  }

  onFocus(e?: Event | any) {
    this.OnFocus.emit(e);
    if(!this.helper) this.helper = {};
    if(!this.helper[this.key]) this.helper[this.key] = {};
    this.helper[this.key].wasFocused = true;

    if (this.validateOn.includes('focus')) {
      this.validate();
    }
  }

  onInput(e?: Event | any) {
    this.OnInput.emit(e);
    if(!this.helper) this.helper = {};
    if(!this.helper[this.key]) this.helper[this.key] = {};
    this.helper[this.key].wasFocused = true;

    if (this.validateOn.includes('input')) {
      this.validate();
    }
  }

  onModelChanged(e?: Event | any) {
    this.OnModelChanged.emit(e);
    if(!this.helper) this.helper = {};
    if(!this.helper[this.key]) this.helper[this.key] = {};
    this.helper[this.key].modelWasChanged = true;

    if (this.validateOn.includes('modelChange')) {
      this.validate();
    }
  }

  public validate() {
    this.validator.updateHelperWithKey(this.key, this.model, this.helper, false);
  }
}