import { IHelper, HelperType, IHelperItem, ValidateOn } from './../validator.service';
import { Component, OnInit, Input, ElementRef, ViewChild, HostListener, OnChanges, SimpleChanges } from '@angular/core';
import { ValidatorService } from '../validator.service';
import { NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { DatepickerI18n } from 'src/app/datepicker.i18n';
import { UtilsService } from 'src/app/services/utils.service';

interface IDateModel {
  year: number,
  month: number,
  day: number
}

interface IHourModel {
  hour: number,
  minute: number,
  second?: number
}

interface IDateTimeModel extends IDateModel {
  hour: number,
  minute: number,
  second?: number
}

@Component({
  selector: 'app-byedatetime',
  templateUrl: './byedatetime.component.html',
  styleUrls: ['./byedatetime.component.scss'],
  providers: [
    {
      provide: NgbDatepickerI18n,
      useClass: DatepickerI18n
    }
  ]
})
export class ByedatetimeComponent implements OnInit, OnChanges {
  @ViewChild('fakeInput') fakeInput: ElementRef<HTMLElement>;
  @ViewChild('calendarBtnWrap') calendarBtnWrap: ElementRef<HTMLElement>;
  @ViewChild('drop') drop: ElementRef<HTMLElement>;

  @Input() model: any;
  @Input() helper: IHelper;
  @Input() key: string;
  @Input() valid: HelperType;
  @Input() showtime = true;
  @Input() showpicker = true;
  @Input() disable = false;
  @Input() validateOn: ValidateOn[] = ['modelChange'];

  declare date_model: IDateModel;
  declare hour_model: IHourModel;
  showDrop: boolean = false;

  dropPosX: 'left' | 'right' = 'right';
  dropPosY: 'top' | 'bottom' = 'bottom';

  constructor(
    private validator: ValidatorService,
    private utils: UtilsService
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    const {previousValue, currentValue} = changes?.model || {};

    if (changes.valid && this.helper && this.key && this.helper[this.key]) {
      const helperItem: IHelperItem = this.utils.copy(this.helper[this.key]);
      
      this.helper[this.key] = {
        type: this.valid,
        valid: helperItem?.valid || 'UNKNOWN',
        hide: helperItem?.hide || false,
        wasFocused: helperItem?.wasFocused || false,
        lostFocus: helperItem?.lostFocus || false,
        wasChanged: helperItem?.wasChanged || false,
        modelWasChanged: helperItem?.modelWasChanged || false,
        isValid: helperItem?.isValid || false,
        isInvalid: helperItem?.isInvalid || false
      }

      if (this.validateOn.includes('modelChange') || this.validateOn.includes('input')) {
        this.validate();
      }
    }
    
    if (previousValue && currentValue && previousValue[this.key] !== currentValue[this.key]) {
      this.changed();
    }

    if (changes.valid && this.helper && this.key && this.helper[this.key]) {
      this.helper[this.key].type = this.valid;
      this.helper[this.key].valid !== 'UNKNOWN' && this.validate();
    }
  }

  ngOnInit() {
    if (this.helper && this.key) {
      this.helper[this.key] = {
        type: this.valid,
        valid: 'UNKNOWN',
        hide: false,
        wasFocused: false,
        lostFocus: false,
        wasChanged: false,
        modelWasChanged: false,
        isValid: false,
        isInvalid: false
      };

      if (this.validateOn.includes('init')) {
        this.validate();
      }
    }
    
    this.changed();
  }

  @HostListener("window:resize", [])
  onWindowResize() {
    this.calculatePosition();
  }

  @HostListener("window:scroll", [])
  onWindowScroll() {
    this.calculatePosition();
  }

  changed() {
    const dateTime: IDateTimeModel = this.computeDateTime(this.model[this.key])
    
    this.date_model = {
      year: dateTime.year,
      month: dateTime.month,
      day: dateTime.day
    }
    
    const dm: IDateModel = this.date_model;

    const y: number = dateTime.year;
    const m: number = dateTime.month;
    const d: number = dateTime.day;

    if (dm.year === y && dm.month === m && dm.day === d) {
      this.hour_model = {
        hour: dateTime.hour,
        minute: dateTime.minute,
        second: dateTime.second
      }
    } else {
      this.hour_model = {
        hour: 0,
        minute: 0,
        second: 0
      }
    }

    this.model[this.key] = this.formatDate(new Date(this.date_model.year, this.date_model.month - 1, this.date_model.day, this.hour_model.hour, this.hour_model.minute, this.hour_model.second || 0));
    this.validate();
  }

  dateChanged(x: IDateModel) {
    const t: Date = this.model[this.key] ? new Date(this.model[this.key]) : new Date();
    const date = new Date(x.year || t.getFullYear(), x.month - 1 || t.getMonth(), x.day || t.getDate(), t.getHours(), t.getMinutes(), t.getSeconds());

    this.model[this.key] = this.formatDate(date);
    this.changed();
  }

  timeChanged(x: IHourModel) {
    const t: Date = this.model[this.key] ? new Date(this.model[this.key]) : new Date();
    const date = new Date(t.getFullYear(), t.getMonth(), t.getDate(), x.hour || t.getHours(), x.minute || t.getMinutes(), x.second || t.getSeconds());

    this.model[this.key] = this.formatDate(date);
    this.changed();
  }

  private formatDate(datetime: Date | string | number | undefined | null): string {
    const dateTime: IDateTimeModel = this.computeDateTime(datetime);

    let x = '';

    x = '' + dateTime.year;
    x += '-';
    if (dateTime.month < 10) { x += '0'; }
    x += dateTime.month;
    x += '-';
    if (dateTime.day < 10) { x += '0'; }
    x += dateTime.day;

    x += ' ';
    if (dateTime.hour < 10) { x += '0'; }
    x += dateTime.hour;
    x += ':';
    if (dateTime.minute < 10) { x += '0'; }
    x += dateTime.minute;

    return x;
  }

  private computeDateTime(datetime: Date | string | number | undefined | null): IDateTimeModel {
    const date = datetime ? new Date(datetime) : new Date();
    
    const dm: IDateModel = {
      year: date.getFullYear(),
      month: date.getMonth() + 1,
      day: date.getDate()
    }

    const y: number = date.getFullYear();
    const m: number = date.getMonth() + 1;
    const d: number = date.getDate();
    let hourModel: IHourModel;

    if (dm.year === y && dm.month === m && dm.day === d) {
      hourModel = {
        hour: date.getHours(),
        minute: date.getMinutes(),
        second: date.getSeconds()
      }
    } else {
      hourModel = {
        hour: 0,
        minute: 0,
        second: 0
      }
    }
    
    return {
      ...dm,
      ...hourModel
    }
  }

  @HostListener('window:click', ['$event'])
  onWindowClick(e: MouseEvent | any) {
    const 
      fakeInput: HTMLElement | undefined = this.getClosestElementFromEvent(e, this.fakeInput?.nativeElement),
      calendarBtnWrap: HTMLElement | undefined = this.getClosestElementFromEvent(e, this.calendarBtnWrap?.nativeElement),
      drop: HTMLElement | undefined = this.getClosestElementFromEvent(e, this.drop?.nativeElement);

    if (!fakeInput && !calendarBtnWrap && !drop) {
      this.showDrop = false;
    }
  }

  getClosestElementFromEvent(e: any, elem: HTMLElement): HTMLElement | undefined {
    let path: HTMLElement[] = [];

    if (e && e.path && e.path.length > 0) {
      path = e.path || [];
    } else if (e.composedPath) {
      path = e.composedPath() || [];
    } 

    if (!path?.length) return undefined;
    return path.find((el: HTMLElement) => el === elem);
  }

  openDrop() {
    this.calculatePosition();
    this.showDrop = true;
  }

  calculatePosition() {
    const width: number = window.innerWidth; // Szerokość okna przeglądarki
    const height: number = window.innerHeight; // Wysokość okna przeglądarki
    const fakeInput: HTMLElement = this.fakeInput.nativeElement; // Falszywy input
    const calendarBtnWrap: HTMLElement = this.calendarBtnWrap.nativeElement; // Dodatek do falszywego inputu z przyciskiem
    if (!fakeInput || !calendarBtnWrap) return;

    const fakeInputRect: DOMRect = fakeInput.getBoundingClientRect();
    const calendarBtnWrapRect: DOMRect = calendarBtnWrap.getBoundingClientRect();
    const posCenterX: number = fakeInputRect.left + (fakeInputRect.width + calendarBtnWrapRect.width) / 2;
    const posCenterY: number = fakeInputRect.top + (fakeInputRect.height + calendarBtnWrapRect.height) / 2;
    
    this.dropPosX = posCenterX > (width / 2) ? 'left' : 'right';
    this.dropPosY = posCenterY > (height / 2) ? 'top' : 'bottom';
  }

  public validate() {
    this.validator.updateHelperWithKey(this.key, this.model, this.helper, false);
  }
}