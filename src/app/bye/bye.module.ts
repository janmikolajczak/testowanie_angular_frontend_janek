// import { ModelNumberDirective } from './model-number.directive';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
// import { ByecheckboxComponent } from './byecheckbox/byecheckbox.component';
// import { ByedateComponent } from './byedate/byedate.component';
// import { ByedatetimeComponent } from './byedatetime/byedatetime.component';
// import { ByegalleryComponent } from './byegallery/byegallery.component';
// import { ByenumberComponent } from './byenumber/byenumber.component';
// import { ByepassComponent } from './byepass/byepass.component';
// import { ByeradioComponent } from './byeradio/byeradio.component';
// import { ByeselectComponent } from './byeselect/byeselect.component';
// import { ByeswitchComponent } from './byeswitch/byeswitch.component';
// import { ByetextComponent } from './byetext/byetext.component';
// import { ByetextareaComponent } from './byetextarea/byetextarea.component';
// import { ByeuploadComponent } from './byeupload/byeupload.component';
// import { ByefilelistComponent } from './byefilelist/byefilelist.component';
// import { ByemapComponent } from './byemap/byemap.component';
// import { ByetooltipComponent } from './byetooltip/byetooltip.component';
// import { NumbersonlyDirective } from './numberfilter.directive';
// import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
// import { HttpLoaderFactory } from '../app.module';
// import { HttpClient } from '@angular/common/http';
// import { NgImageSliderModule } from 'ng-image-slider';
// import { ByesearchSelectComponent } from './byesearch-select/byesearch-select.component';


// @NgModule({
//   declarations: [
//     NumbersonlyDirective,
// 	  ByeswitchComponent,
//     ByecheckboxComponent,
//     ByedateComponent,
//     ByedatetimeComponent,
//     ByedatetimeComponent,
//     ByefilelistComponent,
//     ByenumberComponent,
//     ByeradioComponent,
//     ByeselectComponent,
// 	  ByeswitchComponent,
//     ByetextComponent,
//     ByetextareaComponent,
//     ByeuploadComponent,
//     ByepassComponent,
//     ByegalleryComponent,
// 	  ByemapComponent,
//     ByetooltipComponent,	
//     ModelNumberDirective, 
//     ByesearchSelectComponent
//   ],
//   imports: [
//     CommonModule,
//     FormsModule,
//     ReactiveFormsModule,
//     NgImageSliderModule,
//     TranslateModule.forChild({
//       loader: {
//           provide: TranslateLoader,
//           useFactory: HttpLoaderFactory,
//           deps: [HttpClient]
//       },
//       defaultLanguage: 'pl'
//     }),
//     NgbModule
//   ],
//   exports: [
//     NumbersonlyDirective,
// 	  ByeswitchComponent,
//     ByecheckboxComponent,
//     ByedateComponent,
//     ByedatetimeComponent,
//     ByedatetimeComponent,
//     ByefilelistComponent,
//     ByenumberComponent,
//     ByeradioComponent,
//     ByeselectComponent,
// 	  ByeswitchComponent,
//     ByetextComponent,
//     ByetextareaComponent,
//     ByeuploadComponent,
//     ByepassComponent,
//     ByegalleryComponent,
//   	ByemapComponent,
//     ByetooltipComponent,	
//     TranslateModule,
//     NgbModule,
//     ModelNumberDirective,
//     ByesearchSelectComponent
//   ]
// })
// export class ByeModule { }
