import { Component, Input, ViewChild, HostListener, ContentChild } from '@angular/core';

@Component({
  selector: 'bye-popover',
  templateUrl: './byepopover.component.html',
  styleUrls: ['./byepopover.component.scss']
})
export class ByepopoverComponent {
  @ViewChild('popover') declare popover: HTMLElement | any; // HTML na jaki ma najeżdżać mysz
  @ContentChild('popoverElem') declare popoverElem;
  @ContentChild('popoverContent') declare popoverContent;

  @Input() className: string = ''; // Klasa dla tooltipu
  @Input() windowClassName: string = ''; // Klasa dla okienka tooltipu
  @Input() inline: boolean = true; // Czy tooltip jest inline elementem

  width: number = window.innerWidth; // Szerokość okna przeglądarki
  height: number = window.innerHeight; // Wysokość okna przeglądarki

  constructor() {}

  definePopoverPosition(e: Event | any) {    
    const x = e.x || e.pageX || e.clienX; // współrzędna myszy x
    const y = e.y || e.pageY || e.clienY; // współrzędna myszy x
    
    const wr = this.width - x; // Prawa część ekranu od myszy (rozmiar)
    const wl = x; // Lewa część ekranu od myszy (rozmiar)
    const hb = this.height - y; // Dolna część ekranu od myszy (rozmiar)
    const ht = y; // Górna część ekranu od myszy (rozmiar)

    let w: number;

    // Pozycja tooltipa po X
    if (wr > wl) {
      w = wr > 500 ? 500 : wr; // Szyrokość okna tooltipa
      
      this.popover.nativeElement.style.left = `${wl}px`;
      this.popover.nativeElement.style.right = 'auto';
    } else {
      w = wl > 500 ? 500 : wl; // Szyrokość okna tooltipa
      
      this.popover.nativeElement.style.right = `${wr}px`;
      this.popover.nativeElement.style.left = 'auto';
    }
    
    // Pozycja tooltipa po Y
    if (hb > ht) {
      this.popover.nativeElement.style.top = `${ht}px`;
      this.popover.nativeElement.style.bottom = 'auto';
    } else {
      this.popover.nativeElement.style.bottom = `${hb}px`;
      this.popover.nativeElement.style.top = 'auto';
    }    

    this.popover.nativeElement.style.width = `${w}px`;
  }

  @HostListener('window:resize', [])
  chechWindowSize(): void {
    this.width = window.innerWidth;
    this.height = window.innerHeight;
  }
}
