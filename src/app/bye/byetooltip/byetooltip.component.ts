import { Component, OnInit, Input, ViewChild, HostListener } from '@angular/core';


@Component( {
  selector: 'app-byetooltip',
  templateUrl: './byetooltip.component.html',
  styleUrls: ['./byetooltip.component.scss'],
} )
export class ByetooltipComponent implements OnInit {
  @ViewChild('tooltipContent') tooltipContent: HTMLElement | any; // HTML na jaki ma najeżdżać mysz

  @Input() tooltip: string = ''; // Treść w okienku tooltip
  @Input() className: string = ''; // Klasa dla tooltipu
  @Input() windowClassName: string = ''; // Klasa dla okienka tooltipu
  @Input() inline: boolean = true; // Czy tooltip jest inline elementem

  width: number = window.innerWidth; // Szerokość okna przeglądarki
  height: number = window.innerHeight; // Wysokość okna przeglądarki

  constructor() {}

  ngOnInit() {
    let arr: string[] = (this.tooltip || '').split(' \\n ');
    arr = arr.map(item => `${item}<br />`);
    this.tooltip = arr.join('');
  }

  test(e: Event | any) {    
    const x = e.x || e.pageX || e.clienX; // współrzędna myszy x
    const y = e.y || e.pageY || e.clienY; // współrzędna myszy x
    
    const wr = this.width - x; // Prawa część ekranu od myszy (rozmiar)
    const wl = x; // Lewa część ekranu od myszy (rozmiar)
    const hb = this.height - y; // Dolna część ekranu od myszy (rozmiar)
    const ht = y; // Górna część ekranu od myszy (rozmiar)

    let w: number;

    // Pozycja tooltipa po X
    if (wr > wl) {
      w = wr > 500 ? 500 : wr; // Szyrokość okna tooltipa
      
      this.tooltipContent.nativeElement.style.left = `${wl}px`;
      this.tooltipContent.nativeElement.style.right = 'auto';
    } else {
      w = wl > 500 ? 500 : wr; // Szyrokość okna tooltipa
      
      this.tooltipContent.nativeElement.style.right = `${wr}px`;
      this.tooltipContent.nativeElement.style.left = 'auto';
    }
    
    // Pozycja tooltipa po Y
    if (hb > ht) {
      this.tooltipContent.nativeElement.style.top = `${ht}px`;
      this.tooltipContent.nativeElement.style.bottom = 'auto';
    } else {
      this.tooltipContent.nativeElement.style.bottom = `${hb}px`;
      this.tooltipContent.nativeElement.style.top = 'auto';
    }    

    this.tooltipContent.nativeElement.style.width = `${w}px`;
  }

  @HostListener('window:resize', [])
  chechWindowSize(): void {
    this.width = window.innerWidth;
    this.height = window.innerHeight;
  }
}