import { IMAGE_EXTENSIONS } from './../../models/constants';
import { IHelper } from './../validator.service';
import { Component, OnInit, Input } from '@angular/core';
import { BasicService } from '../../services/basic.service';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { TranslateService } from '@ngx-translate/core';
import { AlertsService } from '../../services/alert.service';

type VisibleType = 'visible' | 'hidden' | 'hover';

@Component({
  selector: 'app-byefilelist',
  templateUrl: './byefilelist.component.html',
  styleUrls: ['./byefilelist.component.scss']
})
export class ByefilelistComponent implements OnInit {

  @Input() model: any;
  @Input() helper: IHelper;
  @Input() key;
  @Input() remover;
  @Input() type;
  @Input() buttontext;
  @Input() confirmquestion: string = '';
  @Input() confirmQuestionDescription: string = '';
  @Input() starturl;
  @Input() deletable = true;
  @Input() visible: VisibleType = 'visible';

  public base: string;

  constructor(private alert: AlertsService, private translator: TranslateService, private http: HttpClient, private basic: BasicService) { }

  ngOnInit() {
    this.base = this.basic.getBaseUrl();
  }

  stringify(x) {
    return JSON.stringify(x);
  }

  callServlet(index) {
    const req = {
      metadata :
      {
        token : this.basic.getToken()
      },
      data :
      {
        id : this.model[this.key][index].id
      }
    };

    this.http.post(this.basic.getBaseUrl() + '/' + this.remover, req).subscribe(res => {
      const status = this.basic.processResponse(res);
      if (status === 'OK') {
        this.model[this.key].splice(index, 1);
      } else {
        this.alert.showError('', status);
      }
    });
  }

  removeFile(index: number) {
    if (this.confirmquestion !== undefined) {
      Swal.fire({
        title: this.confirmquestion ? this.translator.instant(this.confirmquestion) : '',
        text: this.confirmQuestionDescription ? this.translator.instant(this.confirmQuestionDescription) : '',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: this.translator.instant('YES'),
        cancelButtonText: this.translator.instant('NO')
      } as any).then((result) => {
        if (result.value) {
          this.callServlet(index);
        }
      });
    } else {
      this.callServlet(index);
    }
  }

  isImage(extension: string): boolean {
    return IMAGE_EXTENSIONS.includes(extension);
  }
}
