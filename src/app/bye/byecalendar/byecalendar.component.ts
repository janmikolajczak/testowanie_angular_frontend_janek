import { MainUtilsService } from './services/main-utils.service';
import { AdditionalUtilsService } from './services/additional-utils.service';
import { IEvent, ICeilClickEvent, IViewEventsModel, IRemoveEvent, IEventEditorModel } from './models/interfaces';
import { Component, Input, OnInit, OnChanges, SimpleChanges, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { MS_IN_WEEK, MS_IN_DAY } from './models/constants';
import { MonthInt, ViewMode, DayInt } from './models/types';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import I18nCalendarConfigDTO from './dtos/I18nCalendarConfig.dto';


const today = new Date();

@Component({
  selector: 'bye-calendar',
  templateUrl: './byecalendar.component.html',
  styleUrls: ['./byecalendar.component.scss']
})
export class ByeCalendarComponent implements OnInit, OnChanges {
  @ViewChild('viewEventsModal') declare viewEventsModal: ElementRef;
  @ViewChild('confirmRemoveEventModal') declare confirmRemoveEventModal: ElementRef;
  @ViewChild('eventEditorModal') declare eventEditorModal: ElementRef;

  @Input() startDate: {
    year?: number,
    month: number,
    day: number
  } = {
    year: today.getFullYear(),
    month: today.getMonth(),
    day: today.getDate()
  }

  @Input() defaultViewMode: ViewMode = 'month';
  @Input() events: IEvent[] = [];
  @Input() configI18n: I18nCalendarConfigDTO = new I18nCalendarConfigDTO();

  @Output() OnMounte = new EventEmitter<IEvent[]>();
  @Output() OnChange = new EventEmitter<IEvent[]>();
  @Output() OnAdd = new EventEmitter<IEvent>();
  @Output() OnEdit = new EventEmitter<IEvent>();
  @Output() OnRemove = new EventEmitter<IRemoveEvent>();

  year: number = today.getFullYear();
  month: MonthInt = today.getMonth() as MonthInt;
  day: DayInt = today.getDate() as DayInt;

  monthesName: string[] = this.configI18n.monthes;
  private _viewMode: ViewMode = 'month';
  viewEventsModel?: IViewEventsModel;
  viewEventsModalRef?: NgbModalRef;
  confirmRemoveEventModalRef?: NgbModalRef;
  declare eventIdToRemove: string;
  private mounted: boolean = false;
  eventEditorModel: IEventEditorModel | undefined;
  declare eventEditorModalRef: NgbModalRef;
  titleInputTouched: boolean = false;
  declare lastCeilClickEvent: ICeilClickEvent;

  constructor(
    private additionalUtils: AdditionalUtilsService,
    private mainUtils: MainUtilsService,
    private modalService: NgbModal,
    private datePipe: DatePipe
  ) {}

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes)
    if (changes.events) {
      this.eventsChanged();
    }
  }

  ngOnInit(): void {
    this.configI18n = new I18nCalendarConfigDTO(this.configI18n);
    this.viewMode = this.defaultViewMode;
    this.setupStartDate();
  }

  private setupStartDate() {
    let { year, month, day } = this.startDate;
    if (month < 0 || month > 11) throw new Error('Incorrect month. Month must be between 0 and 11');
    if (day < 1 || day > 31) throw new Error('Incorrect day. Day must be between 1 and 31');
    if (!year) year = today.getFullYear();

    const monthDayCount = this.mainUtils.getMonthDayCount(month as MonthInt, year);
    if (day > monthDayCount) throw new Error(`Incorrect day. Month ${month} has ${monthDayCount} days, but you input ${day}`);

    this.year = year;
    this.month = month as MonthInt;
    this.day = day as DayInt;
  }

  get viewMode(): ViewMode {
    return this._viewMode;
  }

  set viewMode(viewMode: ViewMode) {
    this._viewMode = viewMode;
  }

  goToPrev() {
    if (this.viewMode === 'month') {
      this.goToPrevMonth();
    }
    if (this.viewMode === 'week') {
      this.goToPrevWeek();
    }
    if (this.viewMode === 'day') {
      this.goToPrevDay();
    }
  }

  goToNext() {
    if (this.viewMode === 'month') {
      this.goToNextMonth();
    }
    if (this.viewMode === 'week') {
      this.goToNextWeek();
    }
    if (this.viewMode === 'day') {
      this.goToNextDay();
    }
  }

  goToToday() {
    this.year = today.getFullYear();
    this.month = today.getMonth() as MonthInt;
    this.day = today.getDate() as DayInt;
  }

  onCeilClick(e: ICeilClickEvent) {
    this.lastCeilClickEvent = e;
    const date = e.range.from;
    const filteredEvents: IEvent[] = this.mainUtils.filterEvents(
      this.events, 
      date.getTime(),
      e.range.to.getTime()
    );

    this.openViewEventsModal({
      year: date.getFullYear(),
      month: date.getMonth() as MonthInt,
      day: date.getDate() as DayInt,
      events: filteredEvents
    });
  }

  openViewEventsModal(config?: IViewEventsModel) {
    this.viewEventsModel = config || undefined;
    this.viewEventsModalRef = this.modalService.open(this.viewEventsModal, {size: 'lg'});
  }

  goToViewMode(viewMode: ViewMode) {
    if (this.viewEventsModel) {
      this.year = this.viewEventsModel.year;
      this.month = this.viewEventsModel.month;
      this.day = this.viewEventsModel.day;
    }

    this.viewMode = viewMode;
    this.viewEventsModalRef?.close();
  }

  newDate(year: number, month: MonthInt, day: DayInt): Date {
    return new Date(year, month, day);
  }

  openConfirmRemoveEventModal(id?: string) {
    this.eventIdToRemove = id as string;
    this.confirmRemoveEventModalRef = this.modalService.open(this.confirmRemoveEventModal);
  }

  removeEvent() {
    let key: string | number | undefined;

    for (let i = 0, len = this.events.length; i < len; i++) {
      if (key) break;
      
      const e = this.events[i];
      if (e.id === this.eventIdToRemove) {
        key = e.key;
      }
    }

    this.confirmRemoveEventModalRef?.close();

    this.onRemove({
      id: this.eventIdToRemove,
      key
    });
  }

  onMounte() {
    this.OnMounte.emit(this.events);
  }

  onAdd(e: IEvent) {
    this.OnAdd.emit(e);
  }

  onEdit(e: IEvent) {
    this.OnEdit.emit(e);
  }

  onChange() {
    this.OnChange.emit(this.events);
  }

  onRemove(e: IRemoveEvent) {
    this.OnRemove.emit(e);

    this.onChange();
  }

  openEventEditorModal(id?: string) {
    this.titleInputTouched = false;
    const events = this.events;
    let e: IEvent | undefined;
    if (id) {
      for (let i = 0, len = events.length; i < len; i++) {
        if (e) break;

        const event = events[i];
        if (event.id === id) {
          e = JSON.parse(JSON.stringify(event));
          break;
        }
      }
    }
    
    if (e) {
      this.eventEditorModel = e;
    } else {
      const date = new Date(this.viewEventsModel!.year, this.viewEventsModel!.month, this.viewEventsModel!.day)
      const year = date.getFullYear();
      const month = date.getMonth();
      const day = date.getDate();

      this.eventEditorModel = {
        title: '',
        color: this.additionalUtils.getRandomColor(),
        periodType: 'date',
        from: new Date(year, month, day, 0, 0, 0),
        to: new Date(year, month, day, 23, 59, 59)
      }
    }
    
    this.eventEditorModalRef = this.modalService.open(this.eventEditorModal);
  }
  
  setEventEditorModelDate(e: string) {
    const date = new Date(e);
    const year = date.getFullYear();
    const month = date.getMonth();
    const day = date.getDate();

    const from = new Date(year, month, day, 0, 0, 0);
    const to = new Date(year, month, day, 23, 59, 59);
    this.eventEditorModel!.from = from;
    this.eventEditorModel!.to = to;
  }

  setEventEditorModelDateFrom(e: string) {
    const date = new Date(e);
    const year = date.getFullYear();
    const month = date.getMonth();
    const day = date.getDate();

    this.eventEditorModel!.from = new Date(year, month, day, 0, 0, 0);
    const isInstanceOfDate = this.eventEditorModel!.to instanceof Date;
    if (!isInstanceOfDate) this.eventEditorModel!.to = new Date(this.eventEditorModel!.to);

    if (this.eventEditorModel!.to.getTime() < this.eventEditorModel!.from.getTime()) {
      this.eventEditorModel!.to = new Date(year, month, day, 23, 59, 59);
    }
  }

  setEventEditorModelDateTo(e: string) {
    const date = new Date(e);
    const year = date.getFullYear();
    const month = date.getMonth();
    const day = date.getDate();

    this.eventEditorModel!.to = new Date(year, month, day, 23, 59, 59);
    const isInstanceOfDate = this.eventEditorModel!.from instanceof Date;
    if (!isInstanceOfDate) this.eventEditorModel!.from = new Date(this.eventEditorModel!.from);

    if (this.eventEditorModel!.to.getTime() < this.eventEditorModel!.from.getTime()) {
      this.eventEditorModel!.from = new Date(year, month, day, 0, 0, 0);
    }
  }

  setEventEditorModelTime(e: string) {
    this.eventEditorModel!.from = new Date(e);
    this.eventEditorModel!.to = new Date(e);
  }

  setEventEditorModelTimeFrom(e: string) {
    this.eventEditorModel!.from = new Date(e);

    const isInstanceOfDate = this.eventEditorModel!.to instanceof Date;
    if (!isInstanceOfDate) this.eventEditorModel!.to = new Date(this.eventEditorModel!.to);

    if (this.eventEditorModel!.to.getTime() < this.eventEditorModel!.from.getTime()) {
      this.eventEditorModel!.to = new Date(e);
    }
  }

  setEventEditorModelTimeTo(e: string) {
    this.eventEditorModel!.to = new Date(e);

    const isInstanceOfDate = this.eventEditorModel!.from instanceof Date;
    if (!isInstanceOfDate) this.eventEditorModel!.from = new Date(this.eventEditorModel!.from);

    if (this.eventEditorModel!.to.getTime() < this.eventEditorModel!.from.getTime()) {
      this.eventEditorModel!.from = new Date(e);
    }
  }

  saveEvent() {
    if (!this.eventEditorModel!.title?.length) return;

    if (this.eventEditorModel!.id) {
      this.editEvent();
    } else {
      this.addEvent();
    }

    this.eventEditorModalRef.close();
    this.eventEditorModel = undefined;
    this.eventsChanged();
  }

  editEvent() {
    let e: IEvent | undefined;
    
    for (let i = 0, len = this.events.length; i < len; i++) {
      if (e) break;

      if (this.events[i].id === this.eventEditorModel!.id) {
        e = this.events[i];
        break;
      }
    }

    if (!e) throw new Error('Event not found');
    this.onEdit(this.copyEvent(this.eventEditorModel, false));
  }

  addEvent() {
    const e: IEvent = this.copyEvent(this.eventEditorModel!);
    e.id === this.additionalUtils.generateHash();
    
    this.onAdd(e);
  }

  touchTitleInput() {
    this.titleInputTouched = true;
  }

  dateToString(date: Date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd HH:mm') || '';
  }

  public eventsChanged() {
    this.checkEvents();
    this.checkEventsIds();
    this.checkViewEventsModel();

    this.events = [...this.events];

    if (!this.mounted) {
      this.mounted = true;
      this.onMounte();
    } else {
      this.onChange();
    }
  }

  private goToPrevMonth() {
    if (this.month === 0) {
      --this.year;
      this.month = 11;
    } else {
      --this.month;
    }
  }

  private goToNextMonth() {
    if (this.month === 11) {
      ++this.year;
      this.month = 0;
    } else {
      ++this.month;
    }
  }

  private goToPrevWeek() {
    this.changePeriod('week', 'prev');
  }

  private goToNextWeek() {
    this.changePeriod('week', 'next');
  }

  private goToPrevDay() {
    this.changePeriod('day', 'prev');
  }

  private goToNextDay() {
    this.changePeriod('day', 'next');
  }

  private changePeriod(period: 'day' | 'week', type: 'prev' | 'next') {
    const nowDate = new Date(this.year, this.month, this.day);
    let ts: number;

    if (period === 'week') {
      ts = type === 'next' ? nowDate.getTime() + MS_IN_WEEK : nowDate.getTime() - MS_IN_WEEK;
    } else {
      ts = type === 'next' ? nowDate.getTime() + MS_IN_DAY : nowDate.getTime() - MS_IN_DAY;
    }

    const date = new Date(ts);
    this.year = date.getFullYear();
    this.month = date.getMonth() as MonthInt;
    this.day = date.getDate() as DayInt;
  }

  private checkEvents() {
    let hasErr: boolean = false;
    const events: IEvent[] = this.events;

    for (let i = 0, len = events.length; i < len; i++) {
      if (hasErr) break;
      const e = events[i];
      if (e.to.getTime() < e.from.getTime()) {
        hasErr = true;
        break;
      }
    }

    if (hasErr) throw new Error('Param "from" can not be biggest then param "to". Calendar can work incorrect.');
  }

  private checkEventsIds() {
    const events: IEvent[] = this.events;
    const ids = new Set<string>();

    for (let i = 0, len = events.length; i < len; i++) {
      const e = events[i];
      if (!e.id) e.id = this.additionalUtils.generateHash();
      
      ids.add(e.id as string);
    }

    if (ids.size !== events.length) throw new Error('Some event "id" is not unique');
  }

  private checkViewEventsModel() {
    if (!this.viewEventsModel || !this.lastCeilClickEvent?.range) return;

    const date = this.lastCeilClickEvent.range.from;
    const filteredEvents: IEvent[] = this.mainUtils.filterEvents(
      this.events, 
      date.getTime(),
      this.lastCeilClickEvent.range.to.getTime()
    );

    this.viewEventsModel.events = filteredEvents;
  }

  private copyEvent(e: IEvent, setIdIfNone: boolean = true) {
    if (setIdIfNone && !e.id) e.id = this.additionalUtils.generateHash();

    e = JSON.parse(JSON.stringify(e));
    e.from = new Date(e.from);
    e.to = new Date(e.to);

    return e;
  }
}