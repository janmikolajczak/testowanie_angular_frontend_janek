import { IDayCalendarHour, IDayOfMonth, IWeekCalendarHour } from "./interfaces";

export type Day28Int = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26 | 27 | 28;
export type Day29Int = Day28Int | 29;
export type Day30Int = Day29Int | 30;
export type Day31Int = Day30Int | 31;
export type DayInt = Day31Int;
export type MonthInt = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11;
export type DayOfWeekInt = 0 | 1 | 2 | 3 | 4 | 5 | 6;

export type ViewMode = 'month' | 'week' | 'day';

export type MonthCalendarWeek = [IDayOfMonth, IDayOfMonth, IDayOfMonth, IDayOfMonth, IDayOfMonth, IDayOfMonth, IDayOfMonth];
export type MonthCalendarWeeks = [MonthCalendarWeek, MonthCalendarWeek, MonthCalendarWeek, MonthCalendarWeek, MonthCalendarWeek, MonthCalendarWeek];

export type WeekCalendarDay = [
  IWeekCalendarHour, IWeekCalendarHour, IWeekCalendarHour, IWeekCalendarHour,
  IWeekCalendarHour, IWeekCalendarHour, IWeekCalendarHour, IWeekCalendarHour,
  IWeekCalendarHour, IWeekCalendarHour, IWeekCalendarHour, IWeekCalendarHour,
  IWeekCalendarHour, IWeekCalendarHour, IWeekCalendarHour, IWeekCalendarHour,
  IWeekCalendarHour, IWeekCalendarHour, IWeekCalendarHour, IWeekCalendarHour,
  IWeekCalendarHour, IWeekCalendarHour, IWeekCalendarHour, IWeekCalendarHour,
];
export type WeekCalendarDays = [WeekCalendarDay, WeekCalendarDay, WeekCalendarDay, WeekCalendarDay, WeekCalendarDay, WeekCalendarDay, WeekCalendarDay];

export type WeekCalendarRow = [IWeekCalendarHour, IWeekCalendarHour, IWeekCalendarHour, IWeekCalendarHour, IWeekCalendarHour, IWeekCalendarHour, IWeekCalendarHour];
export type WeekCalendarRows = WeekCalendarRow[];

export type DayCalendarDay = [
  IDayCalendarHour, IDayCalendarHour, IDayCalendarHour, IDayCalendarHour,
  IDayCalendarHour, IDayCalendarHour, IDayCalendarHour, IDayCalendarHour,
  IDayCalendarHour, IDayCalendarHour, IDayCalendarHour, IDayCalendarHour,
  IDayCalendarHour, IDayCalendarHour, IDayCalendarHour, IDayCalendarHour,
  IDayCalendarHour, IDayCalendarHour, IDayCalendarHour, IDayCalendarHour,
  IDayCalendarHour, IDayCalendarHour, IDayCalendarHour, IDayCalendarHour,
];
export type DayCalendarRows = IDayCalendarHour[][];

export type EventPeriodType = 'time' | 'date' | 'timerange' | 'daterange';