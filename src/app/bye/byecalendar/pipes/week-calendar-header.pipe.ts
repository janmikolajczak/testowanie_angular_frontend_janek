import { TranslateService } from '@ngx-translate/core';
import { MainUtilsService } from './../services/main-utils.service';
import { MS_IN_DAY, MONTHES_NAME, MONTHES_SHORT_NAME } from './../models/constants';
import { MonthInt, DayInt } from './../models/types';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'weekCalendarHeader'
})
export class WeekCalendarHeaderPipe implements PipeTransform {

  constructor(
    private translateService: TranslateService,
    private mainUtils: MainUtilsService
  ) {}

  transform(_: any, year: number, month: MonthInt, day: DayInt): string {
    const firstWeekDayDate = this.mainUtils.getFirstWeekDayByDay(new Date(year, month, day));
    const lastWeekDayDate = new Date(firstWeekDayDate.getTime() + 6 * MS_IN_DAY);
    
    const d1 = firstWeekDayDate.getDate();
    const d2 = lastWeekDayDate.getDate();
    const m1 = this.translateService.instant(MONTHES_SHORT_NAME[firstWeekDayDate.getMonth()]);
    const m2 = this.translateService.instant(MONTHES_SHORT_NAME[lastWeekDayDate.getMonth()]);
    const y1 = firstWeekDayDate.getFullYear();
    const y2 = lastWeekDayDate.getFullYear();

    return `${d1} ${m1} ${y1} - ${d2} ${m2} ${y2}`
  }
}