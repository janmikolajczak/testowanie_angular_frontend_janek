import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: 'arrayLimit'
})
export class ArrayLimitPipe implements PipeTransform {

  transform(arr: any[], limit: number = Infinity): any[] {
    if (!arr || typeof(arr) !== 'object' || !Array.isArray(arr)) throw new Error('Param "arr" must be an array');

    const a: any[] = [];

    for (let i = 0, len = arr.length; i < len; i++) {
      if (i >= limit) break;
      if (i < limit) {
        a.push(arr[i]);
      }
    }

    return a;
  }
}