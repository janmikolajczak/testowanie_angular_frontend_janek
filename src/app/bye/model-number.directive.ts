import { Directive, EventEmitter, HostBinding, HostListener, Input, Output } from '@angular/core';

@Directive({
  selector: '[byeModelNumber]'
})
export class ModelNumberDirective {
  @HostBinding('value')
  @Input() byeModelNumber: number;

  @Output() byeModelNumberChange: EventEmitter<number> = new EventEmitter<number>();

  constructor() {}

  @HostListener('keyup', ['$event.target.value'])
  onKeyup(val: string) {
    this.byeModelNumberChange.emit(+val);
  }

  @HostListener('change', ['$event.target.value'])
  onChange(val: string) {
    this.byeModelNumberChange.emit(+val);
  }
}