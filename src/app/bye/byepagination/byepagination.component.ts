import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-byepagination',
  templateUrl: './byepagination.component.html',
  styleUrls: ['./byepagination.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ByepaginationComponent implements OnInit {
  @Input() model!: any;
  @Input() total_pages!: number;
  @Input() hideWhenOnePage: boolean = true;

  route: string[] = ['/'];

  constructor() {}

  ngOnInit(): void {
    try { 
      const pathname: string = window.location.pathname;
      console.log(pathname)
      let pathnameArr: string[] = pathname.split('/');
      let _;
      [_, ...pathnameArr] = pathnameArr;
      pathnameArr.filter(path => path !== 'lakepass2');
      
      pathnameArr[0] = '/' + pathnameArr[0];
      this.route = JSON.parse(JSON.stringify(pathnameArr));
    } catch (err) {
      console.error(err);
    }
  }

  getQuery(page: number): string {
    let x = JSON.parse(JSON.stringify(this.model));
    if(x['pagination'] === undefined) x['pagination'] = {};
    x['pagination']['max_page_length'] = undefined;
    x['pagination']['start_page'] = page;
    const y = JSON.stringify(x);
    return y;
  }
}
