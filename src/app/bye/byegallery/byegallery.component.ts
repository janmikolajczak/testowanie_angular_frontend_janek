import { Component, Input, OnInit } from '@angular/core';
import { BasicService } from '../../services/basic.service';
import { IMAGE_EXTENSIONS } from 'src/app/models/constants';

@Component({
  selector: 'app-byegallery',
  templateUrl: './byegallery.component.html',
  styleUrls: ['./byegallery.component.scss']
})
export class ByegalleryComponent implements OnInit {

  constructor(public basic: BasicService) { }
  @Input() data: any;
  @Input() table: string;
  @Input() keepratio: boolean = true;
  @Input() height: string = '50px';
  public imageList: Array<object> = [];
  public fileList: Array<object> = [];

  ngOnInit(): void {
    if(this.data !== undefined && this.data !== null)
    {
      const list = this.data[this.table];

      let n;
      if(list !== undefined) n = list.length;
      else n = 0;
      let i;
      for(i=0;i<n;i++)
      {
        const x = list[i];

        if(IMAGE_EXTENSIONS.includes(x['extension']))
        {
          const y =
          {
            image: this.basic.getBaseUrl() + '/' + this.table + x['file_path'] + '/original' + x['extension'],
            thumbImage: this.basic.getBaseUrl() + '/' + this.table + x['file_path'] + '/miniature' + x['extension'],
            alt: x['original_name'],
            title: ''
          }
          
          this.imageList.push(y);
        }
        else
        {
          const y =
          {
            url: this.basic.getBaseUrl() + '/' + this.table + x['file_path'] + '/original' + x['extension'],
            title: x['original_name']
          };
          this.fileList.push(y);
        }
      }
    }
  }
}
