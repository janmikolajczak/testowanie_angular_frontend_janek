import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import * as L from 'leaflet';
import { icon, Marker } from 'leaflet';
import {LoaderService} from '../../services/loader.service';


@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnDestroy {
    @Output() positionChanged = new EventEmitter<{ lat: number, lng: number }>();

    map: L.Map;
    marker: L.Marker;

    constructor(private loader: LoaderService) {
        this.loader.state = {
            showLoader: false
        };
    }

    ngOnDestroy(): void {
        this.map.off();
        this.map.remove();
        const appMapElement = document.getElementsByTagName('app-map')[0];
        if (appMapElement) {
            appMapElement.parentNode.removeChild(appMapElement);
        }
    }

    ngOnInit(): void {
        this.initMap();
    }

    private initMap(): void {

        this.map = L.map('map', {
            center: [52.1724, 20.8079],
            zoom: 12
        });

        const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 17,
            minZoom: 5,
            attribution: 'OpenStreetMap'
        });

        tiles.addTo(this.map);

        this.map.on('click', (e): void => {
            this.updateMarkerPosition(e.latlng);
        });
        this.fixMarker();
        this.marker = L.marker([52.1724, 20.8079], {draggable: true}).addTo(this.map);
        this.marker.on('dragend', (e): void => {
            this.updateMarkerPosition(e.target.getLatLng());
        });

    }

    private updateMarkerPosition(latlng: L.LatLng) {
        this.marker.setLatLng(latlng);
        this.positionChanged.emit({lat: latlng.lat, lng: latlng.lng});
    }

    private fixMarker() {
        const iconRetinaUrl = 'assets/marker-icon-2x.png';
        const iconUrl = 'assets/marker-icon.png';
        const shadowUrl = 'assets/marker-shadow.png';
        const iconDefault = icon({
            iconRetinaUrl,
            iconUrl,
            shadowUrl,
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34],
            tooltipAnchor: [16, -28],
            shadowSize: [41, 41]
        });
        Marker.prototype.options.icon = iconDefault;
    }
}
