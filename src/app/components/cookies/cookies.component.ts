import { CookieService } from 'src/app/services/cookie.service';
import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-cookies',
  templateUrl: './cookies.component.html',
  styleUrls: ['./cookies.component.scss']
})
export class CookiesComponent implements OnInit, OnDestroy {
  showCookiesModal: boolean = false;
  declare sub$: Subscription;

  constructor(
    private cookieService: CookieService
  ) {}

  ngOnInit(): void {
    this.checkCookies();
  }

  checkCookies() {
		this.sub$ = this.cookieService.all$
      .subscribe(data => {
        this.showCookiesModal = data?.accept_cookies !== 'true';
      });
	}

	accept() {
    const now: Date = new Date();
		this.cookieService.setItem('accept_cookies', 'true', {
      expires: new Date(now.getFullYear() + 1, now.getMonth(), now.getDate())
    });
	}

	ngOnDestroy() {
		this.sub$?.unsubscribe();
	}
}
