import { Component, HostListener, OnInit } from '@angular/core';
import { ColorSchemeService, SelectedColorScheme } from 'src/app/services/color-scheme.service';

@Component({
  selector: 'app-color-scheme-select',
  templateUrl: './color-scheme-select.component.html',
  styleUrls: ['./color-scheme-select.component.scss']
})
export class ColorSchemeSelectComponent implements OnInit {
  colorSchemeDropdownOpen: boolean = false;
  selectedColorScheme: SelectedColorScheme = this.colorSchemaService.selectedColorScheme;

  constructor(
    private colorSchemaService: ColorSchemeService
  ) {}

  ngOnInit(): void {
    this.colorSchemaService.selectedColorScheme = this.colorSchemaService.selectedColorScheme;

    this.colorSchemaService.selectedColorScheme$
      .subscribe(colorScheme => this.selectedColorScheme = colorScheme);
  }

  @HostListener('window:click', ['$event'])
  onWindowClick(e: MouseEvent) {
    let path: HTMLElement[] = (e as any)?.path?.length || [];
    if ((e as any)?.path) {
      path = (e as any)?.path;
    } else if (e.composedPath) {
      path = e.composedPath() as HTMLElement[];
    }
    
    if (!path || path.length === 0) return;
    const dropdown = path.find(x => x?.id === 'color_scheme_dropdown');
    const btn = path.find(x => x?.id === 'color_scheme_btn');
    if (dropdown || btn) return;
    this.colorSchemeDropdownOpen = false;
  }

  selectColorScheme(colorScheme: SelectedColorScheme) {
    this.colorSchemaService.selectedColorScheme = colorScheme;
  }
}
