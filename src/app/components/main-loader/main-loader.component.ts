import { LoaderService } from './../../services/loader.service';
import { Subscription } from 'rxjs';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-main-loader',
  templateUrl: './main-loader.component.html',
  styleUrls: ['./main-loader.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MainLoaderComponent implements OnInit, OnDestroy {
  sub$?: Subscription;
  showLoader: boolean = true;
  loaderWrapClassName: string = '';
  loaderClassName: string = '';

  constructor(
    private loader: LoaderService
  ) {}

  ngOnInit(): void {
    this.subscribeState();
  }

  subscribeState() {
    this.sub$ = this.loader.state$
      .subscribe(state => {
        this.showLoader = state.showLoader;
        this.loaderWrapClassName = state.loaderWrapClassName || '';
        this.loaderClassName = state.loaderClassName || '';
      });
  }

  ngOnDestroy(): void {
    try {
      this.sub$ && this.sub$.unsubscribe();
    } catch (_) {}
  }
}
