import { Directive, HostBinding, Input, Output, EventEmitter, HostListener } from '@angular/core';

@Directive({
  selector: '[appPhone]'
})
export class PhoneDirective {
  @HostBinding('value')
  @Input() appPhone: string;

  @Output() appPhoneChange: EventEmitter<string> = new EventEmitter();

  constructor() {}

  @HostListener('keyup', ['$event'])
  onKeyup(e: Event | any) {
    this.validate(e);
  }

  @HostListener('change', ['$event'])
  onChange(e: Event | any) {
    this.validate(e);
  }

  @HostListener('blur', ['$event'])
  onBlur(e: Event | any) {
    this.validate(e);
  }

  validate(e: Event | any) {
    const allowedSumbols: string[] = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    const val = e.target.value;
    let arr: string[] = val.split('');

    arr = arr.filter((symbol, idx) => {
      symbol = symbol.trim();
      if (idx === 0 && symbol === '+') return true;
      return allowedSumbols.includes(symbol);
    });

    if (arr.length === 11 && arr[0] == '4' && arr[1] == '8') {
      arr = ['+', ...arr];
    } 
    if (arr.length === 9 && arr[0] !== '+') {
      arr = ['+', '4', '8', ...arr];
    }

    this.appPhone = arr.join('');
    e.target.value = this.appPhone;
    this.appPhoneChange.emit(this.appPhone);
  }
}