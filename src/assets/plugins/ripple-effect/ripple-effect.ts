document.body.addEventListener('click', (e: MouseEvent) => {
  try {
    const elem = e.target as HTMLElement;
    const target = (elem.closest('.ripple-effect') || elem.closest('.btn')) as HTMLElement;
    if (!target) return;
    
    const rect = target.getBoundingClientRect();
    const x = e.clientX - rect.x;
    const y = e.clientY - rect.y;

    const ripples = document.createElement('span');
    ripples.classList.add('ripple-effect-span')
    ripples.style.left = x + 'px';
    ripples.style.top = y + 'px';
    target.appendChild(ripples);

    window.setTimeout(() => {
      ripples.remove();
    }, 400);
  } catch (_) {}
});
